#!/bin/bash
#
# ymTkUbuntu16BaseAmiBootstrap.sh
#
# setup Ubuntu public aws image as a base ami candidate for custom builds
#

generalUpdateUbuntu() {
   echo "MESSAGE: $(date) generalUpdateUbuntu. upgrade, upgrade, autoremove."
   sudo DEBIAN_FRONTEND=noninteractive apt-get -y -o DPkg::options::="--force-confdef" -o DPkg::options::="--force-confold" upgrade
   sudo DEBIAN_FRONTEND=noninteractive apt-get -y upgrade
   sudo DEBIAN_FRONTEND=noninteractive apt-get -y update
   sudo DEBIAN_FRONTEND=noninteractive apt-get -y autoremove
}

cloudLogReset() {
   echo "MESSAGE: $(date) cloudLogReset. move cloud-init logs to archived name."
   if [ -f /var/log/cloud-init.log ]; then sudo mv /var/log/cloud-init.log /var/log/cloud-init.log.$$; fi
   if [ -f /var/log/cloud-init-output.log ]; then sudo mv /var/log/cloud-init-output.log /var/log/cloud-init-output.log.$$; fi
}

handyLinks() {
   echo "MESSAGE: $(date) handyLinks for sysadmins."
   if [ ! -L /root/cfn-init-cmd.log ];      then sudo ln -s /var/log/cfn-init-cmd.log                   /root/cfn-init-cmd.log; fi
   if [ ! -L /root/part-001 ];              then sudo ln -s /var/lib/cloud/instance/scripts/part-001    /root/part-001; fi
   if [ ! -L /root/cloud-init.log ];        then sudo ln -s /var/log/cloud-init.log                     /root/cloud-init.log; fi
   if [ ! -L /root/cloud-init-output.log ]; then sudo ln -s /var/log/cloud-init-output.log              /root/cloud-init-output.log; fi
}

removeUpgrades() {
   echo "MESSAGE: $(date) removeUpgrades. Remove for example unattended-upgrades."
   #remove unattended-upgrades & other upgrade packages
   #we are using immuteable infrastrcuture so we do not want the OS to perform any in-place udgrades
   sudo systemctl disable apt-daily.service
   sudo systemctl disable apt-daily.timer
   sed --in-place --follow-symlinks  's/APT::Periodic::Update-Package-Lists \"1\";/APT::Periodic::Update-Package-Lists \"0\";/' /etc/apt/apt.conf.d/10periodic
   for PACKAGE in unattended-upgrades ubuntu-release-upgrader-core python3-distupgrade;
   do
      COUNT=$(sudo apt list --installed 2>/dev/null | grep ${PACKAGE} | grep -v grep | wc -l)
      if [ ${COUNT} -gt 0 ]; then
         sudo DEBIAN_FRONTEND=noninteractive apt-get remove -y ${PACKAGE}
      fi
   done
}

addCfnSupport() {
   echo "MESSAGE: $(date) addCfnSupport"
   if [ ! -f /usr/local/bin/cfn-init ]; then
      sudo DEBIAN_FRONTEND=noninteractive apt-get -y install python-software-properties python-pip python-setuptools
      sudo easy_install https://s3.amazonaws.com/cloudformation-examples/aws-cfn-bootstrap-latest.tar.gz
      sudo pip install awscli
   fi
}

 export DEBIAN_FRONTEND='noninteractive'
 export PATH='/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin:/snap/bin'
 generalUpdateUbuntu
 handyLinks
 cloudLogReset
 removeUpgrades
 addCfnSupport
 generalUpdateUbuntu

#eof
